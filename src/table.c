#include "table.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief allocates memory for a TOS
 *
 * @return Pointer on Tos
 */
Tos *tos_create() {
  Tos *t = malloc(sizeof(Tos));
  t->head = NULL;
  t->size = 0;
  return t;
}

/**
 * @brief allocates memory for a Symbol
 *
 * @return Pointer on Symbol
 */
Symbol *symbol_allocate() {
  Symbol *el = malloc(sizeof(struct s_entry));
  el->id = (char *)malloc(STRING_SIZE * sizeof(char));
  el->type = (char *)malloc(STRING_SIZE * sizeof(char));
  el->value = (char *)malloc(STRING_SIZE * sizeof(char));
  el->scope = (char *)malloc(STRING_SIZE * sizeof(char));

  strcpy(el->id, "");
  strcpy(el->type, "");
  strcpy(el->value, "");
  strcpy(el->scope, "");
  el->next = NULL;

  return el;
}

/**
 * @brief creates a new temporary symbol
 *
 * @return Pointer on Symbol
 */
Symbol *symbol_new_temp() {
  static int cpt = 0;
  char buffer[STRING_SIZE];
  snprintf(buffer, STRING_SIZE, "t%d", cpt);
  cpt++;
  Symbol *s = symbol_allocate();
  strcpy(s->id, buffer);
  return s;
}

/**
 * @brief creates a new int Symbol
 *
 * @param t
 * @param value
 * @param type
 * @param scope
 * @return Pointer on Symbol
 */
Tos *tos_new_int(Tos *t, int value, char *type, char *scope) {
  Symbol *new = symbol_new_temp();
  char buffer[STRING_SIZE];
  snprintf(buffer, STRING_SIZE, "%d", value);
  strcpy(new->value, buffer);
  strcpy(new->type, type);
  strcpy(new->scope, scope);
  t = tos_add_symbol(t, new);
  return t;
}

/**
 * @brief creates a new double Symbol
 *
 * @param t
 * @param value
 * @param type
 * @param scope
 * @return Pointer on Symbol
 */
Tos *tos_new_double(Tos *t, double value, char *type, char *scope) {
  Symbol *new = symbol_new_temp();
  char buffer[STRING_SIZE];
  snprintf(buffer, STRING_SIZE, "%f", value);
  strcpy(new->value, buffer);
  strcpy(new->type, type);
  strcpy(new->scope, scope);
  t = tos_add_symbol(t, new);
  return t;
}

/**
 * @brief creates a new variable Symbol and adds it to the Tos
 *
 * @param t
 * @param type
 * @param id
 * @param value
 * @param scope
 * @return Pointer on Symbol
 */
Tos *tos_new_var(Tos *t, char *type, char *id, char *value, char *scope) {
  Symbol *new = symbol_allocate();
  strcpy(new->type, type);
  strcpy(new->id, id);
  strcpy(new->value, value);
  strcpy(new->scope, scope);
  if (tos_lookup(t, id) != NULL) {
    fprintf(stderr, "Redefintion of variable %s.\n", id);
    exit(1);
  } else {
    t = tos_add_symbol(t, new);
  }
  return t;
}

/**
 * @brief adds a Symbol to the Tos
 *
 * @param t
 * @param s
 * @return Pointer on Tos
 */
Tos *tos_add_symbol(Tos *t, Symbol *s) {
  // if(tos_lookup(t, s->id) == NULL){
  s->next = t->head;
  t->head = s;
  t->size++;
  // }
  return t;
}

/**
 * @brief checks if a specific symbol is in the Tos
 *
 * @param t
 * @param identifier
 * @return Pointer on Symbol if it is in the Tos, NULL otherwise
 */
Symbol *tos_lookup(Tos *t, char *identifier) {
  Symbol *current = t->head;
  while (current != NULL) {
    if (strcmp(current->id, identifier) == 0)
      return current;
    current = current->next;
  }
  return NULL;
}

/**
 * @brief scans the ast to build then return the Tos
 *
 * @param ast
 * @param t
 * @param scope
 * @param type
 * @param decl
 */
void ast_to_tos(ast *ast, Tos **t, char *scope, char *type, int decl) {
  static int stdio_in = 0;
  char buff[STRING_SIZE];
  if (ast == NULL) {
    return;
  }
  switch (ast->type) {
  case AST_BINARY:
    ast_to_tos(ast->left, t, scope, "", decl);
    ast_to_tos(ast->right, t, scope, "", decl);
    break;
  case AST_UNARY:
    ast_to_tos(ast->identifier, t, scope, "", decl);
    break;
  case AST_ID:
    if (decl == 1) {
      *t = tos_new_var(*t, type, ast->id, "/", scope);
    } else {
      if (tos_lookup(*t, ast->id) == NULL) {
        fprintf(stderr, "Variable %s is used but not defined.\n", ast->id);
        exit(1);
      }
    }
    break;
  case AST_INT:
    *t = tos_new_int(*t, ast->number, "int", scope);
    break;
  case AST_DOUBLE:
    *t = tos_new_double(*t, ast->number_double, "double", scope);
    break;
  case AST_STRING:
    break;
  case AST_FOR:
    ast_to_tos(ast->init, t, "for", "", decl);
    ast_to_tos(ast->cond_for, t, "for", "", decl);
    ast_to_tos(ast->step, t, "for", "", decl);
    ast_to_tos(ast->body_for, t, "for", "", decl);
    break;
  case AST_WHILE:
    ast_to_tos(ast->cond_while, t, "while", "", decl);
    ast_to_tos(ast->body_while, t, "while", "", decl);
    break;
  case AST_IF:
    ast_to_tos(ast->cond_if, t, "if", "", decl);
    ast_to_tos(ast->body_if, t, "if", "", decl);
    if (ast->else_body_if)
      ast_to_tos(ast->else_body_if, t, "else", "", decl);
    break;
  case AST_TABLE:
    strcpy(buff, "tab ");
    strcat(buff, type);
    ast_to_tos(ast->tab_id, t, scope, buff, decl);
    ast_to_tos(ast->tab_el, t, scope, type, 0);
    break;
  case AST_TABLE_DECL:
    switch (ast->var_type) {
    case INT:
      ast_to_tos(ast->tab_decl_id, t, scope, "int", 1);
      if (ast->tab_decl_right != NULL) {
        ast_to_tos(ast->tab_decl_right, t, scope, "", 0);
      }
      break;
    case DOUBLE:
      ast_to_tos(ast->tab_decl_id, t, scope, "double", 1);
      if (ast->tab_decl_right != NULL) {
        ast_to_tos(ast->tab_decl_right, t, scope, "", 0);
      }
      break;
    default:
      printf("Unknwon type!");
    }
    break;
  case AST_TABLE_AFFECT:
    ast_to_tos(ast->tab_affect_id, t, scope, "", decl);
    if (ast->tab_affect_right != NULL) {
      ast_to_tos(ast->tab_affect_right, t, scope, "", decl);
    }
    break;
  case AST_DECL:
    switch (ast->var_type) {
    case INT:
      if ((ast->decl_right != NULL) && (ast->decl_right->type == AST_INT)) {
        snprintf(buff, STRING_SIZE, "%d", ast->decl_right->number);
        *t = tos_new_var(*t, "int", ast->decl_id->id, buff, scope);
      } else {
        ast_to_tos(ast->decl_id, t, scope, "int", 1);
        if (ast->decl_right != NULL) {
          ast_to_tos(ast->decl_right, t, scope, "", 0);
        }
      }
      break;
    case DOUBLE:
      if ((ast->decl_right != NULL) && (ast->decl_right->type == AST_DOUBLE)) {
        snprintf(buff, STRING_SIZE, "%f", ast->decl_right->number_double);
        *t = tos_new_var(*t, "double", ast->decl_id->id, buff, scope);
      } else {
        ast_to_tos(ast->decl_id, t, scope, "double", 1);
        if (ast->decl_right != NULL) {
          ast_to_tos(ast->decl_right, t, scope, "", 0);
        }
      }
      break;
    default:
      printf("Unknwon type!");
    }
    break;
  case AST_AFFECT:
    ast_to_tos(ast->affect_id, t, scope, "", decl);
    if (ast->affect_right != NULL) {
      ast_to_tos(ast->affect_right, t, scope, "", decl);
    }
    break;
  case AST_INSTR:
    ast_to_tos(ast->instr, t, scope, "", decl);
    ast_to_tos(ast->instrs, t, scope, "", decl);
    break;
  case AST_GINSTR:
    ast_to_tos(ast->g_instr, t, scope, "", decl);
    ast_to_tos(ast->g_instrs, t, scope, "", decl);
    break;
  case AST_INCLUDE:
    if (ast->includeType == '<' && strcmp(ast->hfile, "stdio.h") == 0) {
      stdio_in = 1;
    }
    break;
  case AST_DEFINE:
    snprintf(buff, STRING_SIZE, "%d", ast->define_value);
    *t = tos_new_var(*t, "int", ast->define_id, buff, "GLOB");
    break;
  case AST_FCT:
    *t = tos_new_var(*t, "fct", ast->fct_name, "/", "GLOB");
    ast_to_tos(ast->args, t, ast->fct_name, "", decl);
    ast_to_tos(ast->bloc, t, ast->fct_name, "", decl);
    break;
  case AST_FCT_ARGS:
    ast_to_tos(ast->args_left, t, scope, type, decl);
    ast_to_tos(ast->args_right, t, scope, type, decl);
    break;
  case AST_FCT_CALL:
    if (strcmp(ast->fct_id->id, "printf") == 0) {
      if (stdio_in == 0) {
        fprintf(stderr,
                "Function printf is used but stdio.h is not included.\n");
        exit(1);
      }
    } else {
      if (tos_lookup(*t, ast->fct_id->id) == NULL) {
        fprintf(stderr, "Function %s is used but not defined.\n",
                ast->fct_id->id);
        exit(1);
      }
    }
    ast_to_tos(ast->fct_args, t, scope, "", decl);
    break;
  case AST_FCT_CALL_ARGS:
    ast_to_tos(ast->call_args_left, t, scope, type, decl);
    ast_to_tos(ast->call_args_right, t, scope, type, decl);
    break;
  case AST_BLOCK:
    ast_to_tos(ast->block, t, scope, "", decl);
    ast_to_tos(ast->return_expr, t, scope, "", decl);
    break;
  default:
    break;
  };
}

/**
 * @brief prints the Tos
 *
 * @param t
 */
void tos_print(Tos *t) {
  printf("+--------------------------------------------------------------------"
         "---------------+\n");
  printf("|         ID         |        TYPE        |        VALUE       |     "
         "    SCOPE      |\n"); // add new columns if necessary
  printf("+--------------------------------------------------------------------"
         "---------------+\n");
  Symbol *current = t->head;
  while (current != NULL) {
    printf("|%-20s|%-20s|%-20s|%-20s|\n", current->id, current->type,
           current->value, current->scope);
    current = current->next;
  }
  printf("+--------------------------------------------------------------------"
         "---------------+\n");
}

/**
 * @brief frees memory allocated for a Symbol
 *
 * @param s
 */
void symbol_free(Symbol *s) {
  free(s->id);
  free(s->type);
  free(s->value);
  free(s->scope);
  free(s);
}

/**
 * @brief frees memory allocated for a Tos
 *
 * @param t
 */
void tos_free(Tos *t) {
  Symbol *current = t->head;
  while (current->next != NULL) {
    Symbol *next = current->next;
    symbol_free(current);
    current = next;
  }
  symbol_free(current);
  free(t);
}
