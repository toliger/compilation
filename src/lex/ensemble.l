%{
  #include "ensemble.tab.h"
%}

%option nounput
%option noyywrap

integer           [-]?[0-9]+
double            [-]?[0-9]+\.[0-9]+
identifier        [a-zA-Z_]([a-zA-Z_]|[0-9])*
header            [a-zA-Z_]([a-zA-Z_]|[0-9])*.h
exist_assign      "+="|"-="|"*="|"/="|"%="
commentv1         "/*"([^*]|\*+[^*/])*\*+"/"
commentv2         "//".*\n
string            ["]([^"\\\n]|\\.|\\\n)+["]

%%
"return"          { return RETURN;}
"int"             { yylval.chaine=strdup(yytext); return INTEGERTYPE;}
"double"          { yylval.chaine=strdup(yytext); return DOUBLETYPE;}
"char"            { yylval.chaine=strdup(yytext); return CHARTYPE;}
"for"             { return FOR;}
"while"           { return WHILE;}
"if"              { return IF;}
"else"            { return ELSE;}
"#define"         { return DEFINE;}
"#include"        { return INCLUDE;}

"="               { return yytext[0];}
"+"               { return yytext[0];}
"-"               { return yytext[0]; }
"*"               { return yytext[0]; }
"/"               { return yytext[0]; }
"=="              { return EQ_SYMB; }
"<"               { return yytext[0]; }
">"               { return yytext[0]; }
"<="              { return LTE_SYMB; }
">="              { return GTE_SYMB; }
"!="              { return NEQ_SYMB; }
{exist_assign}    { yylval.chaine=strdup(yytext); return EXIST_ASSIGN; }
"++"              { return INCR; }
"--"              { return DECR; }
","               { return yytext[0]; }
";"               { return yytext[0]; }
"{"               { return yytext[0]; }
"}"               { return yytext[0]; }
"("               { return yytext[0]; }
")"               { return yytext[0]; }
"["               { return yytext[0]; }
"]"               { return yytext[0]; }
{string}          { yylval.chaine = strdup(yytext); return STRING;}
[ \t\n]           { }
{commentv1}       { }
{commentv2}       { }
<<EOF>>           { return CEOF; }

{integer}         { yylval.int_value = atoi(yytext); return INTEGERVALUE; }
{double}          { yylval.double_value = (double) atof(yytext); return DOUBLEVALUE; }
{identifier}      { yylval.chaine=strdup(yytext); return IDENTIFIER; }
{header}          { yylval.chaine=strdup(yytext); return HEADER; }
.                 { printf("Unrecognized character: %s",yytext);}
%%

void yyerror (char const *msg) {
  fprintf(stderr, "%s\n", msg);
  exit(1);
}

void lex_free() {
  yy_delete_buffer(YY_CURRENT_BUFFER);
  free(yy_buffer_stack);
}
