#include "spec.h"

/* Level 0 */
void int_mulacc(int *acc, int b, int c) { *acc += b * c; }

void double_mulacc(double *acc, double b, double c) { *acc += b * c; }

/* Level 1 - Vec */
void int_vec_set(int lo, int hi, int *v, int val) {
  for (int i = lo; i < hi; i++) {
    v[i] = val;
  }
}

void int_vec_swap(int lo, int hi, int *v1, int *v2) {
  for (int i = lo; i < hi; i++) {
    int tmp = v2[i];
    v2[i] = v1[i];
    v1[i] = tmp;
  }
}

void int_axpy(int lo, int hi, int a, int *x, int *y) {
  for (int i = lo; i < hi; i++) {
    y[i] = a * x[i] + y[i];
  }
}

/* Level 2 - VecMat */

void int_gemv(int lo, int nl, int nc, int alpha, int **mat, int *x, int beta,
              int *y) {
  for (int i = lo; i < nl; i++) {
    y[i] *= beta;
    for (int j = lo; j < nc; j++) {
      y[i] += alpha * mat[i][j] * x[j];
    }
  }
}

/* Level 3 - MatMat */
void int_gemm(int m, int n, int k, int alpha, int **a, int **b, float beta,
              int **c) {
  (void)n;
  (void)k;

  for (int i = 0; i < m; i++) {
    for (int j = 0; j < m; j++) {
      c[i][j] *= beta;
      for (int k = 0; k < m; k++)
        c[i][j] += alpha * a[i][k] * b[k][j];
    }
  }
}
