#include "opti.h"
#include "table.h"
#include "utils.h"
#include <stdlib.h>
#include <string.h>

#define SKIP(b)                                                                \
  b = b->instrs;                                                               \
  continue;
#define CONST(e) (e && (e->type == AST_INT || e->type == AST_DOUBLE))

int checkMulAcc(ast *ast) {
  // start, check if affect
  if (ast->type != AST_AFFECT) {
    return 0;
  }
  struct ast *r_mb = ast->affect_right;
  // check if right member is binary op PLUS
  if (r_mb->type != AST_BINARY || r_mb->op_type != OP_ADD) {
    return 0;
  }
  // op left member should be an ID, equal to the left member
  if (r_mb->left->type != AST_ID) {
    return 0;
  }
  if (strcmp(r_mb->left->id, ast->affect_id->id)) {
    return 0;
  }
  // check op right member, should be binary op MUL
  if (r_mb->right->type != AST_BINARY || r_mb->right->op_type != OP_MUL) {
    return 0;
  }

  // from now on, should be fine
  return 1;
}

void printMulAcc(ast *ast, int is_double) {
  if (is_double) {
    print_out("double_mulacc(&%s, ", ast->affect_id->id);
  } else {
    print_out("int_mulacc(&%s, ", ast->affect_id->id);
  }
  ast_print(ast->affect_right->right->left, 0, 0);
  print_out(", ");
  ast_print(ast->affect_right->right->right, 0, 0);
  print_out(");\n");
}

/**
 * @brief checks if there is a dependancy of iter in the sub-ast of start
 *
 * @param iter
 * @param start
 * @return int
 */
int checkDependancy(ast *iter, ast *start) {
  if (start == NULL || iter == NULL) {
    return 0;
  }
  switch (start->type) {
  case AST_ID:
    if (!strcmp(iter->id, start->id)) {
      return 1;
    }
    break;
  case AST_TABLE:
    if (iter->type == AST_TABLE) { // compare tab_id and tab_id
      if (!strcmp(iter->tab_id->id, start->tab_el->id) ||
          !strcmp(iter->tab_id->id, start->tab_id->id)) {
        return 1;
      }
    } // compare id and tab_id
    if (!strcmp(iter->id, start->tab_el->id) ||
        !strcmp(iter->id, start->tab_id->id)) {
      return 1;
    }
    break;
  case AST_BINARY:
    return 0 + checkDependancy(iter, start->left) +
           checkDependancy(iter, start->right);
  default:
    break;
  }
  return 0;
}

/**
 * @brief checks if there is a vector in the sub-ast
 *
 * @param start
 * @return int
 */
int checkVec(ast *start) {
  if (start == NULL) {
    return 0;
  }
  switch (start->type) {
  case AST_TABLE:
    return 1;
  case AST_BINARY:
    return 0 + checkVec(start->left) + checkVec(start->right);
  default:
    break;
  }
  return 0;
}

ast *getTabID(ast *tab) {
  ast *t = tab;
  while (t->tab_id->type == AST_TABLE) {
    t = t->tab_id;
  }
  if (t->tab_id->type != AST_ID)
    return NULL;
  return t;
}

/**
 * @brief checks if pattern "id = something"
 *
 * @param start
 * @return int
 */
struct valset checkValSet(ast *start, int allow_tab) {
  struct valset res;
  res.left = NULL;
  res.right = NULL;
  switch (start->type) {
  case AST_AFFECT:
    res.left = start->affect_id;
    res.right = start->affect_right;
    break;
  case AST_DECL:
    res.left = start->decl_id;
    res.right = start->decl_right;
    break;
  case AST_TABLE_AFFECT:
    if (!allow_tab)
      break;
    res.left = start->tab_affect_id;
    res.right = start->tab_affect_right;
    break;
  case AST_TABLE_DECL:
    if (!allow_tab)
      break;
    res.left = start->tab_decl_id;
    res.right = start->tab_decl_right;
    break;
  default:
    break;
  }
  return res;
}

int isVec(ast *start) {
  if (start->type != AST_TABLE) {
    return 0;
  }
  if (start->tab_id->type == AST_ID) {
    return 1;
  }
  return 0;
}

struct mat2D_idx isMat(ast *start) {
  struct mat2D_idx res;

  res.li = NULL;
  res.ni = NULL;
  if (start->type == AST_TABLE) {           // 1D
    if (start->tab_id->type == AST_TABLE) { // 2D
      if (start->tab_id->tab_id->type == AST_ID) {
        res.li = start->tab_id->tab_el;
        res.ni = start->tab_el;
      }
    }
  }
  return res;
}

int idcmp(ast *a, ast *b) {
  if (!a || !b) {
    return 0;
  }
  if (a->type != AST_ID || b->type != AST_ID) {
    return 0;
  }
  return !strcmp(a->id, b->id);
}

int veccmp(ast *a, ast *b) {
  if (!isVec(a) || !isVec(b))
    return 0;
  if (a->tab_el->type == AST_INT && b->tab_el->type == AST_INT) {
    return (idcmp(a->tab_id, b->tab_id) &&
            a->tab_el->number == b->tab_el->number);
  }
  return (idcmp(a->tab_id, b->tab_id) && idcmp(a->tab_el, b->tab_el));
}

int checkUnary(ast *start, unary_op_t op) {
  return (start && start->type == AST_UNARY && start->unary_op_type == op);
}

int checkBinary(ast *start, binary_op_t op) {
  return (start && start->type == AST_BINARY && start->op_type == op);
}

struct opti_args checkLevel1(ast *ast) {
  struct opti_args res;
  res.type = BLAS_NOOPTI;
  // should start with for
  if (ast->type != AST_FOR) {
    return res;
  }
  struct ast *init = ast->init, *cond = ast->cond_for, *step = ast->step,
             *body = ast->body_for, *cur_instr, *main_vec, *iterator, *tmp,
             *bis_vec, *r;
  int dep = 0;
  char *tmp_nom;

  tmp = NULL;
  bis_vec = tmp;
  tmp = bis_vec;

  if (init->type == AST_AFFECT) {
    iterator = init->affect_id;
    res.lo = init->affect_right;
  } else if (init->type == AST_DECL) {
    iterator = init->decl_id;
    res.lo = init->decl_right;
  } else {
    return res;
  }

  // check cond
  if (!checkBinary(cond, OP_LT)) {
    return res;
  }
  res.hi = cond->right;

  // check step
  if (checkUnary(step, OP_INCR)) {
  } else {
    return res;
  }

  // check body
  if (body->type != AST_INSTR) {
    return res;
  }
  while (body != NULL) {
    cur_instr = body->instr;

    if (cur_instr->type == AST_TABLE_AFFECT) {
      main_vec = cur_instr->tab_affect_id->tab_id;
      r = cur_instr->affect_right;

      // no dep -> vec_set
      if (dep == 0 && body && !checkDependancy(iterator, r)) {
        if (body->instrs)
          return res;
        res.type = BLAS_VECSET;
        res.vecset_v = main_vec;
        res.vecset_val = r;
        return res;
      }

      // todo: check lookup order
      if (checkBinary(r, OP_ADD)) {
        if (r->right->type == AST_TABLE &&
            !strcmp(r->right->tab_affect_id->id, main_vec->id)) {
          if (checkBinary(r->left, OP_MUL)) {
            r = r->left;
            // check right member of mult
            if (r->right->type == AST_TABLE) {
              // no vectors should be in the left member
              if (!checkVec(r->left) && !checkDependancy(iterator, r->left)) {
                res.type = BLAS_AXPY;
                res.axpy_y = main_vec;
                res.axpy_x = r->right->tab_id;
                res.axpy_a = r->left;
                return res;
              }
            }
          }
        }
      }
    }
    body = body->instrs;
    dep++;
  }

  body = ast->body_for;
  // is vecSwap ? We will see ...
  if (body != NULL) {
    cur_instr = body->instr;
    if (cur_instr->type == AST_AFFECT) {
      tmp_nom = cur_instr->affect_id->id;
      r = cur_instr->affect_right;
      if (r->type == AST_TABLE) {
        tmp = r;
        body = body->instrs;
        if (body != NULL) {
          cur_instr = body->instr;
          if (cur_instr->type == AST_TABLE_AFFECT &&
              checkDependancy(tmp, cur_instr->affect_id)) {
            if (cur_instr->right->type == AST_TABLE) {
              bis_vec = cur_instr->right->tab_id;
              main_vec = cur_instr->tab_affect_id->tab_id;
              body = body->instrs;
              if (body != NULL) {
                cur_instr = body->instr;
                if (cur_instr->type == AST_TABLE_AFFECT &&
                    checkDependancy(bis_vec, cur_instr->affect_id)) {
                  if (!strcmp(cur_instr->right->id, tmp_nom)) {
                    // this boy is a vecswap
                    res.type = BLAS_VECSWAP;
                    res.vecswap_v1 = main_vec;
                    res.vecswap_v2 = bis_vec;
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  return res;
}

int is_type(ast *arg, char *type) {
  Symbol *sym = NULL;
  ast *id;
  if (arg->type == AST_TABLE) {
    id = getTabID(arg); // support of multi-dim arrays
    if (id)
      id = id->tab_id;
  } else {
    id = arg;
  }
  if (id) {
    sym = tos_lookup(table, id->id);
  }
  if (!sym)
    return 0;
  return !strcmp(sym->type, type);
}

int printOpti(struct opti_args args) {
  switch (args.type) {
  case BLAS_VECSET:
    if (is_type(args.vecset_v, "tab double")) {
      print_out("double_vec_set(");
    } else {
      print_out("int_vec_set(");
    }
    ast_print(args.lo, 0, 0);
    print_out(", ");
    ast_print(args.hi, 0, 0);
    print_out(", ");
    ast_print(args.vecset_v, 0, 0);
    print_out(", ");
    ast_print(args.vecset_val, 0, 0);
    print_out(");\n");
    break;
  case BLAS_VECSWAP:
    if (is_type(args.vecswap_v1, "tab double") ||
        is_type(args.vecswap_v2, "tab double")) {
      print_out("double_vec_swap(");
    } else {
      print_out("int_vec_swap(");
    }
    ast_print(args.lo, 0, 0);
    print_out(", ");
    ast_print(args.hi, 0, 0);
    print_out(", ");
    ast_print(args.vecswap_v1, 0, 0);
    print_out(", ");
    ast_print(args.vecswap_v2, 0, 0);
    print_out(");\n");
    break;
  case BLAS_AXPY:
    if (is_type(args.axpy_y, "tab double")) {
      print_out("double_axpy(");
    } else {
      print_out("int_axpy(");
    }
    ast_print(args.lo, 0, 0);
    print_out(", ");
    ast_print(args.hi, 0, 0);
    print_out(", ");
    ast_print(args.axpy_a, 0, 0);
    print_out(", ");
    ast_print(args.axpy_x, 0, 0);
    print_out(", ");
    ast_print(args.axpy_y, 0, 0);
    print_out(");\n");
    break;
  case BLAS_GEMV:
    if (is_type(args.gemv_y, "tab double")) {
      print_out("double_gemv(");
    } else {
      print_out("int_gemv(");
    }
    ast_print(args.lo, 0, 0);
    print_out(", ");
    ast_print(args.hi, 0, 0);
    print_out(", ");
    ast_print(args.hi2, 0, 0);
    print_out(", ");
    ast_print(args.gemv_a, 0, 0);
    print_out(", ");
    ast_print(args.gemv_m, 0, 0);
    print_out(", ");
    ast_print(args.gemv_x, 0, 0);
    print_out(", ");
    ast_print(args.gemv_b, 0, 0);
    print_out(", ");
    ast_print(args.gemv_y, 0, 0);
    print_out(");\n");
    break;
  case BLAS_GEMM:
    if (is_type(args.gemm_c, "tab tab double")) {
      print_out("double_gemm(");
    } else {
      print_out("int_gemm(");
    }
    ast_print(args.gemm_m, 0, 0);
    print_out(", ");
    ast_print(args.gemm_n, 0, 0);
    print_out(", ");
    ast_print(args.gemm_k, 0, 0);
    print_out(", ");
    ast_print(args.gemm_alpha, 0, 0);
    print_out(", ");
    ast_print(args.gemm_a, 0, 0);
    print_out(", ");
    ast_print(args.gemm_b, 0, 0);
    print_out(", ");
    ast_print(args.gemm_beta, 0, 0);
    print_out(", ");
    ast_print(args.gemm_c, 0, 0);
    print_out(");\n");
    break;
  default:
    break;
  }
  return 1;
}

struct opti_args checkLevel3(ast *ast) {
  struct opti_args res;
  res.type = BLAS_NOOPTI;

  struct ast *init = ast->init, *cond = ast->cond_for, *step = ast->step,
             *body = ast->body_for, *cur_instr, *instr1, *iterator, *iterator2,
             *iterator3;
  struct valset vs;
  (void)iterator;

  /* CHECK 1st for */
  vs = checkValSet(init, 0);
  if (vs.left == NULL || vs.right == NULL)
    return res;

  iterator = vs.left;
  res.lo = vs.right;

  // check cond
  if (cond->type != AST_BINARY || cond->op_type != OP_LT)
    return res;
  res.gemm_m = cond->right;

  // check step
  if (step->type == AST_UNARY) {
    if (step->unary_op_type != OP_INCR) {
      return res;
    }
  } else {
    return res;
  }

  cur_instr = body->instr;

  // check second for
  if (cur_instr->type != AST_FOR)
    return res;
  res.gemm_n = cur_instr->cond_for->right;
  vs = checkValSet(cur_instr->init, 0);
  iterator2 = vs.left;
  body = cur_instr->body_for;
  if (body->instrs == NULL || body->instrs->instr->type != AST_FOR)
    return res;
  res.gemm_k = body->instrs->instr->cond_for->right;

  vs = checkValSet(body->instrs->instr->init, 0);
  iterator3 = vs.left;
  cur_instr = body->instr;
  instr1 = cur_instr->right;
  if (checkDependancy(iterator, instr1) || checkDependancy(iterator2, instr1) ||
      checkDependancy(iterator3, instr1))
    return res;
  res.gemm_beta = cur_instr->right;

  cur_instr = body->instrs->instr->body_for->instr;

  res.gemm_alpha = cur_instr->right->left->left;
  if (checkDependancy(iterator, res.gemm_alpha) ||
      checkDependancy(iterator2, res.gemm_alpha) ||
      checkDependancy(iterator3, res.gemm_alpha))
    return res;
  res.gemm_c = cur_instr->tab_affect_id->tab_id->tab_id;
  res.gemm_a = cur_instr->right->left->right->tab_affect_id->tab_id;
  res.gemm_b = cur_instr->right->right->tab_affect_id->tab_id;

  if (idcmp(res.gemm_a, res.gemm_b) || idcmp(res.gemm_a, res.gemm_c) ||
      idcmp(res.gemm_b, res.gemm_c))
    return res;

  res.type = BLAS_GEMM;

  return res;
}

struct opti_args checkLevel2(ast *ast) {
  struct opti_args res;
  res.type = BLAS_NOOPTI;
  int has_temp = 0;
  int has_bmul = 0;

  struct ast *init = ast->init, *cond = ast->cond_for, *step = ast->step,
             *body = ast->body_for, *body2 = NULL, *cur_instr = NULL,
             *cur_instr2 = NULL, *main_vec = NULL, *iterator = NULL,
             *iterator2 = NULL, *r = NULL, *tmp = NULL, *tmp_val = NULL;
  struct valset vs;
  res.gemv_m = NULL;
  res.gemv_b = NULL;
  res.gemv_x = NULL;
  res.gemv_a = NULL;
  res.gemv_y = NULL;

  /* CHECK 1st for */
  vs = checkValSet(init, 0);
  if (vs.left != NULL && vs.right != NULL) {
    iterator = vs.left;
    res.lo = vs.right;
  } else {
    return res;
  }

  // check cond
  if (!checkBinary(cond, OP_LT)) {
    return res;
  }
  res.hi = cond->right;

  // check step
  if (!checkUnary(step, OP_INCR)) {
    return res;
  }
  /* END CHECK 1st for */

  /* CHECK 1st for body */
  if (body->type != AST_INSTR) {
    return res;
  }
  while (body != NULL) {
    cur_instr = body->instr;

    // check tmp val set or mult
    vs = checkValSet(cur_instr, 1);
    // only if mat op has not been detected yet
    if (!res.gemv_m && !has_bmul && !has_temp && vs.left && vs.right &&
        vs.right->type == AST_BINARY) {
      if (isVec(vs.left)) {
        if (veccmp(vs.right->left, vs.left)) {
          tmp_val = vs.right->right;
          has_bmul = 1;
          tmp = vs.left;
          SKIP(body)
        } else if (veccmp(vs.right->right, vs.left)) {
          tmp_val = vs.right->left;
          tmp = vs.left;
          has_bmul = 1;
          SKIP(body)
        }
        if (CONST(vs.left->tab_el)) {
          has_temp = 1;
          tmp_val = vs.right->right;
          tmp = vs.left;
          SKIP(body)
        }
      }
      if (isVec(vs.right->left)) {
        has_temp = 1;
        tmp_val = vs.right->right;
        tmp = vs.left;
        SKIP(body)
      }
      if (isVec(vs.right->right)) {
        has_temp = 1;
        tmp_val = vs.right->left;
        tmp = vs.left;
        SKIP(body)
      }
    }

    /* CHECK 2nd for */
    if (cur_instr->type == AST_FOR && tmp_val) {
      cond = cur_instr->cond_for;
      init = cur_instr->init;
      step = cur_instr->step;
      body2 = cur_instr->body_for;
      vs = checkValSet(init, 0);
      if (vs.left != NULL && vs.right != NULL) {
        iterator2 = vs.left;
      } else {
        return res;
      }

      // check cond
      if (!checkBinary(cond, OP_LT)) {
        return res;
      }
      res.hi2 = cond->right;

      // check step
      if (!checkUnary(step, OP_INCR)) {
        return res;
      }

      if (body2->type != AST_INSTR) {
        return res;
      }
      while (body2 != NULL) {
        cur_instr2 = body2->instr;
        vs = checkValSet(cur_instr2, 1);
        // left member shoudl be vector y
        if (!isVec(vs.left)) {
          return res;
        }
        if (has_bmul && !veccmp(tmp, vs.left)) {
          return res;
        }
        main_vec = vs.left;
        res.gemv_m = NULL;
        res.gemv_y = main_vec;
        // check right member of op
        r = vs.right;

        /*  a M[i][j] x[j] */
        if (checkBinary(r, OP_MUL)) {
          /* a M[i][j] */
          struct ast *rm = r->right;
          struct ast *lm = r->left;
          if (checkBinary(lm, OP_MUL)) {

            // no deps on a with i and j
            if (!checkDependancy(iterator, lm->left) &&
                !checkDependancy(iterator2, lm->left)) {
              res.gemv_a = r->left->left;
            }
            struct mat2D_idx m2dx = isMat(lm->right);
            // check if M is a matrix with correct indexes
            if (m2dx.li != NULL) {
              if (idcmp(m2dx.li, iterator) && idcmp(iterator2, m2dx.ni)) {
                res.gemv_m = lm->right;
              }
            }
          }
          if (isVec(rm)) {
            if (idcmp(rm->tab_id, tmp->tab_id)) {
              return res;
            }
            if (idcmp(iterator2, rm->tab_el)) {
              res.gemv_x = rm;
            }
          }
        }
        body2 = body2->instrs;
      }
    }
    /* END CHECK 2nd for */

    // 1st for
    if (res.gemv_m && has_temp) { // matrix op detected
      vs = checkValSet(cur_instr, 1);
      if (vs.left && isVec(vs.left)) {
        if (veccmp(vs.left, res.gemv_y)) { // is vector y
          // check right part

          if (checkBinary(vs.right, OP_ADD)) { // is ADD

            if (veccmp(vs.right->left, vs.left)) {
              if (idcmp(vs.right->right, tmp)) { // check with id
                res.gemv_b = tmp_val;
              }
              if (veccmp(vs.right->right, tmp)) { // check with vec
                res.gemv_b = tmp_val;
              }
            } else if (veccmp(vs.right->right, vs.left)) {
              if (idcmp(vs.right->left, tmp)) {
                res.gemv_b = tmp_val;
              }
              if (veccmp(vs.right->left, tmp)) { // check with vec
                res.gemv_b = tmp_val;
              }
            } else { // reset
              res.gemv_m = NULL;
            }
          } else { // affect but not add, reset
            res.gemv_m = NULL;
          }
        }
      }
    } else if (has_bmul && !body->instrs) {
      res.gemv_b = tmp_val;
    } else {
      return res;
    }

    // last checks
    if (checkDependancy(iterator, res.gemv_b) ||
        checkDependancy(iterator2, res.gemv_b)) {
      res.gemv_b = NULL;
    }

    body = body->instrs;
  }
  /* END CHECK 1st for body */

  // replace vars by their id
  if (res.gemv_a && res.gemv_b && res.gemv_m && res.gemv_x && res.gemv_y) {
    res.type = BLAS_GEMV;
    res.gemv_m = res.gemv_m->tab_id->tab_id;
    res.gemv_x = res.gemv_x->tab_id;
    res.gemv_y = res.gemv_y->tab_id;
    // res.gemv_b = res.gemv_b->tab_id;
  }
  return res;
}
