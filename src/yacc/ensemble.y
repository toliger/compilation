%{
  #include "table.h"
  #include "utils.h"
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <unistd.h>
  #include <getopt.h>
  int yylex();
  FILE* yyin;
  void yyerror(char*);
  void lex_free();
  void checkExist(char*);
  int fin = 0;
  ast *out;
%}

%union{
  char* chaine;
  int int_value;
  double double_value;
  struct ast* ast;
  char chr;
}

%token RETURN COMPARATOR
%token EOL CEOF DECR INCR
%token <int_value> INTEGERVALUE
%token <double_value>  DOUBLEVALUE
%token <chaine> IDENTIFIER INTEGERTYPE DOUBLETYPE CHARTYPE EQ_SYMB NEQ_SYMB LTE_SYMB GTE_SYMB EXIST_ASSIGN HEADER STRING
%type  <chaine> type exist_assign '='
%type  <chr> '<' '>'
%type  <ast> number expr expr_for expr_while condition step init affect instrs instr declaration expr_if table table_element g_instr g_instrs block axiom args function_call call_args
%token QUIT FOR IF ELSE WHILE
%token INCLUDE DEFINE

%left '+' '-'
%left '*' '/'
%left ','

%%
/** Fichier **/
axiom             : g_instrs CEOF { out = $1; fin = 1; return 0;}
                  ;

g_instrs          : g_instr g_instrs {$$ = ast_new_ginstrs($1, $2);}
                  | { $$ = NULL; }
                  ;

/** Block Main obligatoire **/
g_instr           : type IDENTIFIER '(' args ')' '{' block '}' { $$ = ast_new_fct($1, $2, $4, $7); }
                  | INCLUDE STRING {$$=ast_new_include('"',$2);}
                  | INCLUDE '<' HEADER '>' {$$=ast_new_include('<',$3);}
                  | DEFINE IDENTIFIER INTEGERVALUE {$$=ast_new_define($2,$3);}
                  ;


block             : instrs RETURN expr ';' { $$ = ast_new_block($1, $3); }
                  ;

instrs            : instr instrs {$$ = ast_new_instrs($1,$2); }
                  | { $$ = NULL; }
                  ;


instr             : expr ';' {$$=$1;}
                  | declaration ';' {$$=$1;}
                  | affect ';' {$$=$1;}
                  | expr_for {$$=$1;}
                  | expr_if {$$=$1;}
                  | expr_while { $$ = $1;}
                  ;

function_call     : IDENTIFIER '(' call_args ')' { $$ = ast_new_fct_call(ast_new_id($1),$3); }
                  ;

call_args         : call_args ',' call_args { $$ = ast_new_fct_call_args($1, $3); }
                  | IDENTIFIER { $$ = ast_new_id($1); }
                  | number { $$ = $1; }
                  | STRING { $$ = ast_new_string($1); }
                  | { $$ = NULL; }
                  ;

args              : args ',' args { $$ = ast_new_fct_args($1, $3); }
                  | type IDENTIFIER { $$ = ast_new_decl(str2type($1), ast_new_id($2), NULL); free($1); }
                  | { $$ = NULL; }
                  ;

affect            : IDENTIFIER exist_assign expr {/*checkExist($1);*/ $$ = ast_new_affect(ast_new_id($1),$2,$3);}
                  | table exist_assign expr { $$ = ast_new_table_affect($1, $2, $3); }
                  ;

declaration       : type IDENTIFIER           { $$ = ast_new_decl(str2type($1), ast_new_id($2), NULL); free($1); }
                  | type IDENTIFIER '=' expr  { $$ = ast_new_decl(str2type($1), ast_new_id($2), $4); free($1); }
                  | type table                { $$ = ast_new_table_decl(str2type($1), $2, NULL); free($1);}
                  | type table '=' expr       { $$ = ast_new_table_decl(str2type($1), $2, $4); free($1);}
                  ;


exist_assign      : '='            { $$ = strdup("="); }
                  | EXIST_ASSIGN   { $$ = $1; }
                  ;


expr              : expr '+' expr   { $$ = ast_new_operation(OP_ADD,$1,$3);}
                  | expr '-' expr   { $$ = ast_new_operation(OP_MINUS,$1,$3); }
                  | expr '*' expr   { $$ = ast_new_operation(OP_MUL,$1,$3); }
                  | expr '/' expr   { $$ = ast_new_operation(OP_DIV,$1,$3); }
                  | IDENTIFIER INCR { $$ = ast_new_operation_unary(OP_INCR, ast_new_id($1));}
                  | table INCR      { $$ = ast_new_operation_unary(OP_INCR, $1);}
                  | IDENTIFIER DECR { $$ = ast_new_operation_unary(OP_DECR, ast_new_id($1));}
                  | table DECR      { $$ = ast_new_operation_unary(OP_DECR, $1); }
                  | '(' expr ')'    { $$ = $2;}
                  | IDENTIFIER      { /*checkExist($1);*/ $$=ast_new_id($1);}
                  | table           { $$ = $1; }
                  | number          { $$ = $1;}
                  | function_call   { $$ = $1;}
                  ;


table             : table '[' table_element ']' { $$ = ast_new_table_id($1, $3); }
                  | IDENTIFIER '[' table_element ']' { $$ = ast_new_table_id(ast_new_id($1),$3); }
                  ;

table_element     : IDENTIFIER { $$ = ast_new_id($1); }
                  | number     { $$ = $1; }
                  ;

number            : DOUBLEVALUE { $$=ast_new_double($1); }
                  | INTEGERVALUE { $$=ast_new_integer($1); }
                  ;

type              : INTEGERTYPE { $$ = $1;}
                  | DOUBLETYPE  { $$ = $1;}
                  | CHARTYPE    { $$ = $1;}
                  ;

init              : type IDENTIFIER '=' expr { $$=ast_new_decl(str2type($1), ast_new_id($2), $4); free($1);}
                  | affect {$$=$1;}
                  ;

condition         : expr EQ_SYMB expr {$$ = ast_new_operation(OP_EQ, $1, $3);}
                  | expr NEQ_SYMB expr {$$ = ast_new_operation(OP_NEQ, $1, $3);}
                  | expr GTE_SYMB expr {$$ = ast_new_operation(OP_GTE, $1, $3);}
                  | expr LTE_SYMB expr {$$ = ast_new_operation(OP_LTE, $1, $3);}
                  | expr '>'  expr {$$ = ast_new_operation(OP_GT, $1, $3);}
                  | expr '<'  expr {$$ = ast_new_operation(OP_LT, $1, $3);}
                  | {$$ = NULL;}
                  ;

step              : IDENTIFIER exist_assign expr {/*checkExist($1);*/ $$ = ast_new_affect(ast_new_id($1), $2, $3);}
                  | IDENTIFIER INCR {/*checkExist($1);*/ $$ = ast_new_operation_unary(OP_INCR, ast_new_id($1));}
                  | IDENTIFIER DECR {/*checkExist($1);*/ $$ = ast_new_operation_unary(OP_DECR, ast_new_id($1));}
                  ;

expr_for          : FOR '(' init ';' condition ';' step ')' '{' instrs '}' {$$ = ast_new_for($3, $5, $7, $10);}
                  ;

expr_if           : IF '(' condition ')' '{' instrs '}' {$$ = ast_new_if($3, $6, NULL);}
                  | IF '(' condition ')' '{' instrs '}' ELSE '{' instrs '}' {$$ = ast_new_if($3, $6, $10);}
                  ;

expr_while        : WHILE '(' condition ')' '{' instrs '}' { $$ = ast_new_while($3,$6); }
                  ;


%%

/**
 * @brief usage
 *
 */
void print_usage(){
  fprintf(stderr, "Usage: blaster [--help] [--version] [--tos] [--ast] [-o] <name>\n");
  exit(EXIT_FAILURE);
}

/**
 * @brief help
 *
 */
void print_help(){
  printf("Usage: blaster [--version] [--tos] [--ast] [-o] <in_file>\n");
  printf("OPTIONS:\n");
  printf("  -h --help   :   Displays all the commands the program accepts and what they do.\n");
  printf("  --version   :   Displays the name of all the developers.\n");
  printf("  --tos       :   Displays the symbols table.\n");
  printf("  --ast       :   Displays the AST.\n");
  printf("  -o <out_file>   :   Writes the final code in the file <out_file>.\n");
  exit(EXIT_SUCCESS);
}


int main(int argc, char* argv[]) {
  int opt = 0;
  char filepath[256];
  outstream = stdout;
  FILE *outfile;
  static struct option longoptions[] = {
    {"version", no_argument,       0, 'v' },
    {"tos",     no_argument,       0,  't' },
    {"ast",     no_argument,       0,  'a' },
    {"help",    no_argument,       0,  'h' },
    {0, 0, 0, 0}
  };

  int option_index = 0;
  int o_ast = 0;
  int o_table = 0;
  int o_code_print = 0;
  while ((opt = getopt_long(argc, argv, "o:h:av", longoptions, &option_index)) != -1) {
    switch (opt)
    {
    case 'v':
      printf("Developers:\n\t");
      printf("Cyril LAJARGE\n\tPaul HENG\n\tAdrien OSSWA\n\tTimothée OLIGER.\n");
      printf("Compilation Project Master 1 2019/2020.\n");
      printf("Project is licensed under the GNU General Public License v3.0.\n");
      exit(EXIT_SUCCESS);
    case 't':
      o_table = 1;
      break;
    case 'a':
      o_ast = 1;
      break;
    case 'o':
      strcpy(filepath, optarg);
      printf("Writing final code in file %s.\n", filepath);
      o_code_print = 1;
      break;
    case 'h':
      print_help();
      break;
    default:
      print_usage();
      break;
    }
  }

  if (argc - optind > 0) {
    yyin = fopen(argv[optind], "r");
  }

  table = tos_create();

  while (!fin){
    yyparse();
  }

  ast_to_tos(out, &table, "main", "", 0);
  if (o_table){
    tos_print(table);
    printf("TOS size: %d\n", table->size);
  }

  if(o_ast){
    ast_print(out, 0, 1);
  }

  // printf("int main(){\n");
  if(o_code_print){
    outfile = fopen(filepath, "w");
    outstream = outfile;
    print_out("#include \"spec.h\"\n");
  }

  ast_print(out, 0, 0);
  // print_out("\treturn 0;\n}\n");
  if (o_code_print && !optimized) {
    fseek(outstream, 0, SEEK_SET);
    print_out("// 0 optims found\n");
  }

  // Memory release
  if (table->size != 0)
    tos_free(table);

  ast_free(out);
  lex_free();
  if (o_code_print) {
    fclose(outfile);
  }
  // free(yylval.chaine);
  // ast_free(yylval.ast);
  // free(out);
  // yydestroy();

  return 0;

}


