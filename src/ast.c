#include "ast.h"
#include "opti.h"
#include "table.h"
#include "utils.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * @brief creates a new ast of type AST_BINARY
 *
 * @param type
 * @param left
 * @param right
 * @return Pointer on created AST
 */
ast *ast_new_operation(binary_op_t type, ast *left, ast *right) {
  ast *new = malloc(sizeof(ast));
  new->op_type = type;
  new->type = AST_BINARY;
  new->left = left;
  new->right = right;
  return new;
}

/**
 * @brief creates a new ast of type AST_UNARY
 *
 * @param type
 * @param identifier
 * @return Pointer on created AST
 */
ast *ast_new_operation_unary(unary_op_t type, ast *identifier) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_UNARY;
  new->unary_op_type = type;
  new->identifier = identifier;
  return new;
}

/**
 * @brief creates a new ast of type AST_FOR
 *
 * @param init
 * @param cond
 * @param step
 * @param body
 * @return Pointer on created AST
 */
ast *ast_new_for(ast *init, ast *cond, ast *step, ast *body) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_FOR;
  new->init = init;
  new->cond_for = cond;
  new->step = step;
  new->body_for = body;
  return new;
}

/**
 * @brief creates a new ast of type AST_WHILE
 *
 * @param cond
 * @param body
 * @return Pointer on created AST
 */
ast *ast_new_while(ast *cond, ast *body) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_WHILE;
  new->cond_while = cond;
  new->body_while = body;
  return new;
}

/**
 * @brief creates a new ast of type AST_IF
 *
 * @param cond
 * @param body
 * @param body_else
 * @return Pointer on created AST
 */
ast *ast_new_if(ast *cond, ast *body, ast *body_else) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_IF;
  new->cond_if = cond;
  new->body_if = body;
  new->else_body_if = body_else;
  return new;
}

/**
 * @brief creates a new ast of type AST_INT
 *
 * @param number
 * @return Pointer on created AST
 */
ast *ast_new_integer(int number) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_INT;
  new->number = number;
  return new;
}

/**
 * @brief creates a new ast of type AST_DOUBLE
 *
 * @param number
 * @return Pointer on created AST
 */
ast *ast_new_double(double number) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_DOUBLE;
  new->number_double = number;
  return new;
}

/**
 * @brief creates a new ast of type AST_STRING
 *
 * @param value
 * @return Pointer on created AST
 */
ast *ast_new_string(char *value) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_STRING;
  new->string = value;
  return new;
}

/**
 * @brief creates a new ast of type AST_ID
 *
 * @param id
 * @return Pointer on created AST
 */
ast *ast_new_id(char *id) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_ID;
  new->id = id;
  return new;
}

/**
 * @brief creates a new ast of type AST_TABLE
 *
 * @param id
 * @param el
 * @return Pointer on created AST
 */
ast *ast_new_table_id(ast *id, ast *el) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_TABLE;
  new->tab_id = id;
  new->tab_el = el;
  return new;
}

/**
 * @brief creates a new ast of type AST_TABLE_DECL
 *
 * @param type
 * @param id
 * @param right
 * @return Pointer on created AST
 */
ast *ast_new_table_decl(var_type_t type, ast *id, ast *right) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_TABLE_DECL;
  new->tab_type = type;
  new->tab_decl_id = id;
  new->tab_decl_right = right;
  return new;
}

/**
 * @brief creates a new ast of type AST_TABLE_AFFECT
 *
 * @param id
 * @param op
 * @param right
 * @return Pointer on created AST
 */
ast *ast_new_table_affect(ast *id, char *op, ast *right) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_TABLE_AFFECT;
  new->tab_affect_id = id;
  new->tab_affect_op = op;
  new->tab_affect_right = right;
  return new;
}

/**
 * @brief creates a new ast of type AST_AFFECT
 *
 * @param id
 * @param op
 * @param right
 * @return Pointer on created AST
 */
ast *ast_new_affect(ast *id, char *op, ast *right) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_AFFECT;
  new->affect_id = id;
  new->affect_op = op;
  new->affect_right = right;
  return new;
}

/**
 * @brief creates a new ast of type AST_DECL
 *
 * @param type
 * @param id
 * @param right
 * @return Pointer on created AST
 */
ast *ast_new_decl(var_type_t type, ast *id, ast *right) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_DECL;
  new->var_type = type;
  new->decl_id = id;
  new->decl_right = right;
  return new;
}

var_type_t str2type(char *s) {
  if (strcmp(s, "int") == 0) {
    return INT;
  }
  return DOUBLE;
}

char *generateVariable() {
  static int value = 0;
  char *var = malloc(BUFFER_SIZE * sizeof(char));
  sprintf(var, "t%d", value);
  value++;
  return var;
}

/**
 * @brief creates a new ast of type AST_INSTR
 *
 * @param instr
 * @param instrs
 * @return Pointer on created AST
 */
ast *ast_new_instrs(ast *instr, ast *instrs) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_INSTR;
  new->instr = instr;
  new->instrs = instrs;
  return new;
}

/**
 * @brief creates a new ast of type AST_DEFINE
 *
 * @param id
 * @param value
 * @return Pointer on created AST
 */
ast *ast_new_define(char *id, int value) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_DEFINE;
  new->define_id = id;
  new->define_value = value;
  return new;
}

/**
 * @brief creates a new ast of type AST_INCLUDE
 *
 * @param include_type
 * @param hfile
 * @return Pointer on created AST
 */
ast *ast_new_include(char include_type, char *hFile) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_INCLUDE;
  new->includeType = include_type;
  new->hfile = hFile;
  return new;
}

/**
 * @brief creates a new ast of type AST_GINSTR
 *
 * @param ginstr
 * @param ginstrs
 * @return Pointer on created AST
 */
ast *ast_new_ginstrs(ast *ginstr, ast *ginstrs) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_GINSTR;
  new->g_instr = ginstr;
  new->g_instrs = ginstrs;
  return new;
}

/**
 * @brief creates a new ast of type AST_FCT
 *
 * @param type
 * @param name
 * @param args
 * @param bloc
 * @return Pointer on created AST
 */
ast *ast_new_fct(char *type, char *name, ast *args, ast *bloc) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_FCT;
  new->fct_type = type;
  new->fct_name = name;
  new->args = args;
  new->bloc = bloc;
  return new;
}

/**
 * @brief creates a new ast of type AST_FCT_ARGS
 *
 * @param left
 * @param right
 * @return Pointer on created AST
 */
ast *ast_new_fct_args(ast *left, ast *right) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_FCT_ARGS;
  new->args_left = left;
  new->args_right = right;
  return new;
}

/**
 * @brief creates a new ast of type AST_FCT_CALL
 *
 * @param id
 * @param args
 * @return Pointer on created AST
 */
ast *ast_new_fct_call(ast *id, ast *args) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_FCT_CALL;
  new->fct_id = id;
  new->fct_args = args;
  return new;
}

/**
 * @brief creates a new ast of type AST_FCT_CALL_ARGS
 *
 * @param left
 * @param right
 * @return Pointer on created AST
 */
ast *ast_new_fct_call_args(ast *left, ast *right) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_FCT_CALL_ARGS;
  new->call_args_left = left;
  new->call_args_right = right;
  return new;
}

/**
 * @brief creates a new ast of type AST_BLOCK
 *
 * @param block
 * @param return_expr
 * @return Pointer on created AST
 */
ast *ast_new_block(ast *block, ast *return_expr) {
  ast *new = malloc(sizeof(ast));
  new->type = AST_BLOCK;
  new->block = block;
  new->return_expr = return_expr;
  return new;
}

char *bop2str(binary_op_t type) {
  static char buf[8];
  int k = 0;
  switch (type) {
  case OP_ADD:
    buf[k++] = '+';
    break;
  case OP_MINUS:
    buf[k++] = '-';
    break;
  case OP_DIV:
    buf[k++] = '/';
    break;
  case OP_MUL:
    buf[k++] = '*';
    break;
  case OP_LT:
    buf[k++] = '<';
    break;
  case OP_GT:
    buf[k++] = '>';
    break;
  case OP_LTE:
    buf[k++] = '<';
    buf[k++] = '=';
    break;
  case OP_GTE:
    buf[k++] = '>';
    buf[k++] = '=';
    break;
  case OP_EQ:
    buf[k++] = '=';
    buf[k++] = '=';
    break;
  case OP_NEQ:
    buf[k++] = '!';
    buf[k++] = '=';
    break;
  default:
    buf[k++] = 'N';
    buf[k++] = '/';
    buf[k++] = 'A';
  }
  buf[k++] = 0;
  return buf;
}

char *vt2str(var_type_t type) {
  switch (type) {
  case INT:
    return "int ";
    break;
  case DOUBLE:
    return "double ";
    break;
  default:
    return "unknown_type ";
  }
}

char *uop2str(unary_op_t type) {
  switch (type) {
  case OP_INCR:
    return "++";
    break;
  case OP_DECR:
    return "--";
    break;
  default:
    return "N/A";
  }
}

void print_end_instr(out_t out_type) {
  switch (out_type) {
  case OUT_CODE:
    print_out(";\n");
    break;
  case OUT_NOCR:
    print_out("; ");
    break;
  case OUT_NOSEMI:
    break;
  default:
    break;
  }
}

void print_indent(int nb) {
  for (int i = 0; i < nb; i++)
    print_out("\t");
}

/**
 * @brief prints the AST in form of an AST or code depending of out_type
 *
 * @param ast
 * @param indent
 * @param out_type
 */
void ast_print(ast *ast, int indent, out_t out_type) {
  Symbol *sym;
  struct opti_args opti;
  if (!ast) {
    return;
  }
  print_indent(indent);
  switch (ast->type) {
  case AST_BINARY:
    if (out_type == OUT_AST) {
      printf("%s\n", bop2str(ast->op_type));
      ast_print(ast->left, indent + 1, out_type);
      ast_print(ast->right, indent + 1, out_type);
    } else {
      ast_print(ast->left, 0, out_type);
      print_out(" %s ", bop2str(ast->op_type));
      ast_print(ast->right, 0, out_type);
    }
    break;
  case AST_UNARY:
    if (out_type == OUT_AST) {
      printf("%s\n", uop2str(ast->unary_op_type));
      ast_print(ast->identifier, indent + 1, out_type);
    } else {
      ast_print(ast->identifier, 0, out_type);
      print_out("%s", uop2str(ast->unary_op_type));
      print_end_instr(out_type);
    }
    break;
  case AST_ID:
    if (out_type == OUT_AST) {
      printf("ID (%s)\n", ast->id);
    } else {
      print_out("%s", ast->id);
    }
    break;
  case AST_TABLE:
    if (out_type == OUT_AST) {
      ast_print(ast->tab_id, indent + 1, out_type);
      ast_print(ast->tab_el, indent + 1, out_type);
    } else {
      ast_print(ast->tab_id, 0, out_type);
      print_out("[");
      ast_print(ast->tab_el, 0, out_type);
      print_out("]");
    }
    break;
  case AST_TABLE_AFFECT:
    if (out_type == OUT_AST) {
      ast_print(ast->tab_affect_id, indent + 1, out_type);
      ast_print(ast->affect_right, indent + 1, out_type);
    } else {
      ast_print(ast->tab_affect_id, 0, out_type);
      print_out(" %s ", ast->tab_affect_op);
      if (ast->affect_right != NULL) {
        ast_print(ast->affect_right, 0, out_type);
      }
      print_end_instr(out_type);
    }
    break;
  case AST_TABLE_DECL:
    if (out_type == OUT_AST) {
      printf("decl_tab %s\n", vt2str(ast->var_type));
      ast_print(ast->tab_decl_id, indent + 1, out_type);
      ast_print(ast->tab_decl_right, indent + 1, out_type);
    } else {
      print_out("%s", vt2str(ast->var_type));
      ast_print(ast->tab_decl_id, 0, out_type);
      if (ast->affect_right != NULL) {
        print_out(" = ");
        ast_print(ast->tab_affect_right, 0, out_type);
      }
      print_end_instr(out_type);
    }
    break;
  case AST_INT:
    if (out_type == OUT_AST) {
      printf("INT (%d)\n", ast->number);
    } else {
      print_out("%d", ast->number);
    }
    break;
  case AST_DOUBLE:
    if (out_type == OUT_AST) {
      printf("DOUBLE (%f)\n", ast->number_double);
    } else {
      print_out("%f", ast->number_double);
    }
    break;
  case AST_STRING:
    if (out_type == OUT_AST) {
      printf("STRING (%s)\n", ast->string);
    } else {
      print_out("%s", ast->string);
    }
    break;
  case AST_FOR:
    if (out_type == OUT_AST) {
      printf("for\n");
      ast_print(ast->init, indent + 1, out_type);
      ast_print(ast->cond_for, indent + 1, out_type);
      ast_print(ast->step, indent + 1, out_type);
      ast_print(ast->body_for, indent + 1, out_type);
    } else {
      opti = checkLevel3(ast);
      if (!opti.type) {
        opti = checkLevel2(ast);
      }
      if (!opti.type) {
        opti = checkLevel1(ast);
      }
      if (opti.type) {
        printOpti(opti);
        optimized = 1;
      } else {
        print_out("for(");
        ast_print(ast->init, 0, OUT_NOCR);
        ast_print(ast->cond_for, 0, out_type);
        print_out(";");
        ast_print(ast->step, 0, OUT_NOSEMI);
        print_out(") {\n");
        ast_print(ast->body_for, indent, out_type);
        print_indent(indent);
        print_out("}\n");
      }
    }
    break;
  case AST_WHILE:
    if (out_type == OUT_AST) {
      printf("while\n");
      ast_print(ast->cond_while, indent + 1, out_type);
      ast_print(ast->body_while, indent + 1, out_type);
    } else {
      print_out("while(");
      ast_print(ast->cond_while, 0, out_type);
      print_out(") {\n");
      ast_print(ast->body_while, indent, out_type);
      print_out("\n");
      print_indent(indent);
      print_out("}\n");
    }
    break;
  case AST_IF:
    if (out_type == OUT_AST) {
      printf("if\n");
      ast_print(ast->cond_if, indent + 1, out_type);
      ast_print(ast->body_if, indent + 1, out_type);
      if (ast->else_body_if)
        ast_print(ast->else_body_if, indent + 1, out_type);
    } else {
      print_out("if(");
      ast_print(ast->cond_if, 0, out_type);
      print_out("){\n");
      ast_print(ast->body_if, indent, out_type);
      print_indent(indent);
      if (ast->else_body_if) {
        print_out("} else {\n");
        ast_print(ast->else_body_if, indent, out_type);
      }
      print_indent(indent);
      print_out("}\n");
    }
    break;
  case AST_DECL:
    if (out_type == OUT_AST) {
      printf("decl %s\n", vt2str(ast->var_type));
      ast_print(ast->decl_id, indent + 1, out_type);
      ast_print(ast->decl_right, indent + 1, out_type);
    } else {
      print_out("%s", vt2str(ast->var_type));
      ast_print(ast->decl_id, 0, out_type);
      if (ast->decl_right != NULL) {
        print_out(" = ");
        ast_print(ast->decl_right, 0, out_type);
      }
      print_end_instr(out_type);
    }
    break;
  case AST_AFFECT:
    if (out_type == OUT_AST) {
      printf("affect\n");
      ast_print(ast->affect_id, indent + 1, out_type);
      ast_print(ast->affect_right, indent + 1, out_type);
    } else {
      if (checkMulAcc(ast)) {
        if ((sym = tos_lookup(table, ast->affect_id->id)) == NULL) {
          break;
        }
        if (strcmp(sym->type, "double") == 0) {
          printMulAcc(ast, 1);
        } else {
          printMulAcc(ast, 0);
        }
      } else {
        ast_print(ast->affect_id, 0, out_type);
        if (ast->affect_right != NULL) {
          print_out(" %s ", ast->affect_op);
          ast_print(ast->affect_right, 0, out_type);
        }
        print_end_instr(out_type);
      }
    }
    break;
  case AST_INSTR:
    if (out_type == OUT_AST) {
      printf("INSTR\n");
      ast_print(ast->instr, indent + 1, out_type);
      ast_print(ast->instrs, indent + 1, out_type);
      if (ast->instrs == NULL) {
        // print_out("NULL\n");
      }
    } else {
      ast_print(ast->instr, indent + 1, out_type);
      ast_print(ast->instrs, indent, out_type);
    }
    break;
  case AST_GINSTR:
    if (out_type == OUT_AST) {
      printf("GINSTR\n");
      ast_print(ast->g_instr, indent + 1, out_type);
      ast_print(ast->g_instrs, indent + 1, out_type);
      if (ast->g_instrs == NULL) {
        // print_out("NULL\n");
      }
    } else {
      ast_print(ast->g_instr, indent, out_type);
      ast_print(ast->g_instrs, indent, out_type);
    }
    break;
  case AST_INCLUDE:
    if (out_type == OUT_AST) {
      printf("INCLUDE %s\n", ast->hfile);
    } else {
      if (ast->includeType == '"') {
        print_out("#include %s\n", ast->hfile);
      } else {
        print_out("#include <%s>\n", ast->hfile);
      }
    }
    break;
  case AST_DEFINE:
    if (out_type == OUT_AST) {
      printf("DEFINE %s %d\n", ast->define_id, ast->define_value);
    } else {
      print_out("#define %s %d\n", ast->define_id, ast->define_value);
    }
    break;
  case AST_FCT:
    if (out_type == OUT_AST) {
      printf("%s\n", ast->fct_name);
      ast_print(ast->args, indent + 1, out_type);
      ast_print(ast->bloc, indent + 1, out_type);
    } else {
      print_out("%s %s(", ast->fct_type, ast->fct_name);
      ast_print(ast->args, indent, out_type);
      print_out("){\n");
      ast_print(ast->bloc, indent, out_type);
    }
    break;
  case AST_FCT_ARGS:
    if (out_type == OUT_AST) {
      printf("ARGS\n");
      ast_print(ast->args_left, indent + 1, out_type);
      ast_print(ast->args_right, indent + 1, out_type);
    } else {
      ast_print(ast->args_left, 0, OUT_NOSEMI);
      print_out(",");
      ast_print(ast->args_right, 0, OUT_NOSEMI);
    }
    break;
  case AST_FCT_CALL:
    if (out_type == OUT_AST) {
      printf("FCT CALL\n");
      ast_print(ast->fct_id, indent + 1, out_type);
      ast_print(ast->fct_args, indent + 1, out_type);
    } else {
      ast_print(ast->fct_id, 0, out_type);
      print_out("(");
      ast_print(ast->fct_args, 0, out_type);
      print_out(");\n");
    }
    break;
  case AST_FCT_CALL_ARGS:
    if (out_type == OUT_AST) {
      printf("ARGS\n");
      ast_print(ast->call_args_left, indent + 1, out_type);
      ast_print(ast->call_args_right, indent + 1, out_type);
    } else {
      ast_print(ast->call_args_left, 0, out_type);
      print_out(",");
      ast_print(ast->call_args_right, 0, out_type);
    }
    break;
  case AST_BLOCK:
    if (out_type == OUT_AST) {
      printf("BLOCK\n");
      ast_print(ast->block, indent + 1, out_type);
      ast_print(ast->return_expr, indent + 1, out_type);
    } else {
      ast_print(ast->block, 0, out_type);
      print_indent(1);
      print_out("return ");
      ast_print(ast->return_expr, 0, out_type);
      print_out(";\n}\n");
    }
    break;
  default:
    break;
  }
}

/**
 * @brief frees the memory allocated for AST
 *
 * @param ast
 * @return Pointer on created AST
 */
void ast_free(ast *ast) {
  if (!ast) {
    return;
  }
  switch (ast->type) {
  case AST_BINARY:
    ast_free(ast->left);
    ast_free(ast->right);
    free(ast);
    break;
  case AST_UNARY:
    ast_free(ast->identifier);
    free(ast);
    break;
  case AST_ID:
    free(ast->id);
    free(ast);
    break;
  case AST_TABLE:
    ast_free(ast->tab_id);
    ast_free(ast->tab_el);
    free(ast);
    break;
  case AST_TABLE_AFFECT:
    ast_free(ast->tab_affect_id);
    free(ast->tab_affect_op);
    if (ast->tab_affect_right != NULL) {
      ast_free(ast->tab_affect_right);
    }
    free(ast);
    break;
  case AST_TABLE_DECL:
    ast_free(ast->tab_decl_id);
    if (ast->tab_decl_right != NULL) {
      ast_free(ast->tab_decl_right);
    }
    free(ast);
    break;
  case AST_INT:
    free(ast);
    break;
  case AST_DOUBLE:
    free(ast);
    break;
  case AST_STRING:
    free(ast->string);
    free(ast);
    break;
  case AST_FOR:
    ast_free(ast->cond_for);
    ast_free(ast->init);
    ast_free(ast->step);
    ast_free(ast->body_for);
    free(ast);
    break;
  case AST_WHILE:
    ast_free(ast->cond_while);
    ast_free(ast->body_while);
    free(ast);
    break;
  case AST_IF:
    ast_free(ast->cond_if);
    ast_free(ast->body_if);
    if (ast->else_body_if != NULL) {
      ast_free(ast->else_body_if);
    }
    free(ast);
    break;
  case AST_DECL:
    ast_free(ast->decl_id);
    if (ast->decl_right != NULL) {
      ast_free(ast->decl_right);
    }
    free(ast);
    break;
  case AST_AFFECT:
    ast_free(ast->affect_id);
    free(ast->affect_op);
    if (ast->affect_right != NULL) {
      ast_free(ast->affect_right);
    }
    free(ast);
    break;
  case AST_INSTR:
    ast_free(ast->instr);
    ast_free(ast->instrs);
    free(ast);
    break;
  case AST_GINSTR:
    ast_free(ast->g_instr);
    ast_free(ast->g_instrs);
    free(ast);
    break;
  case AST_INCLUDE:
    free(ast->hfile);
    free(ast);
    break;
  case AST_DEFINE:
    free(ast->define_id);
    free(ast);
    break;
  case AST_FCT:
    ast_free(ast->args);
    ast_free(ast->bloc);
    free(ast->fct_type);
    free(ast->fct_name);
    free(ast);
    break;
  case AST_FCT_ARGS:
    ast_free(ast->args_left);
    ast_free(ast->args_right);
    free(ast);
    break;
  case AST_FCT_CALL:
    ast_free(ast->fct_id);
    ast_free(ast->fct_args);
    free(ast);
    break;
  case AST_FCT_CALL_ARGS:
    ast_free(ast->call_args_left);
    ast_free(ast->call_args_right);
    free(ast);
    break;
  case AST_BLOCK:
    ast_free(ast->block);
    ast_free(ast->return_expr);
    free(ast);
    break;
  default:
    break;
  }
};
