#include "utils.h"

FILE *outstream;
char optimized = 0;

void print_out(const char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vfprintf(outstream, fmt, ap);
  fflush(outstream);
  va_end(ap);
}
