#include <stdio.h>
#define M 16
#define N 8

int main() {
  int labonnemat[M][N];
  int lebonvec[N];
  int y[N];
  int i;
  int j;
  for (i = 0; i < M; i++) {
    for (j = 0; j < N; j++) {
      labonnemat[i][j] = 420;
    }
  }
  int plop = 13;
  int tmp;
  for (i = 0; i < M; i++) {
    tmp = y[i] * plop;
    for (j = 0; j < N; j++) {
      y[i] = ((2 * labonnemat[i][j]) * lebonvec[j]);
    }
    y[i] = y[i] + tmp;
  }
  return 0;
}
