#include <stdio.h>
#define M 16
#define N 8

int main() {
  int mat[M][N];
  int vec[N];
  int y[N];
  int tab[10];
  int i;
  int j;
  int plop = 13;
  int bar = 21;
  int tmp;

  // no opti
  for (i = 0; i < M; i++) {
    int bob = y[i] * plop;
    for (j = 0; j < N; j++) {
      y[i] = ((2 * mat[i][j]) * vec[j]);
    }
    y[i] = y[i] + tmp;
  }

  // no opti
  for (i = 0; i < M; i++) {
    tmp = y[i] * bar;
    int smh = y[i] * plop;
    // tmp = 0;
    for (j = 0; j < N; j++) {
      y[i] = ((2 * mat[i][j]) * vec[j]);
    }
    y[i] = y[i] + smh;
  }

  // opti
  for (i = 0; i < M; i++) {
    tab[1] = y[i] * plop;
    for (j = 0; j < N; j++) {
      y[i] = ((2 * mat[i][j]) * vec[j]);
    }
    y[i] = y[i] + tab[1];
  }

  // no opti
  for (i = 0; i < M; i++) {
    y[i] = (10 + 10 * i - 1) * y[i];
    for (j = 0; j < N; j++) {
      y[i] = ((2 * mat[i][j]) * vec[j]);
    }
  }

  // no opti
  for (i = 0; i < M; i++) {
    y[i] = 4 * 10 * y[i];
    for (j = 0; j < N; j++) {
      y[i] = ((2 * mat[i][j]) * vec[j]);
    }
    y[i]++;
  }

  return 0;
}
