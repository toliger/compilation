#include <stdio.h>
#define N 16

int putchar(int k) {
  return k;
}

int main() {
  int i;
  int ty[N];
  int tx[N];
  for(i = 0; i < N/2; i++) {
    putchar(85);
    tx[i] = 1;
    ty[i] = 5 + 2;
  }
  return 0;
}
