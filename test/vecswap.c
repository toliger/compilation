#include <stdio.h>
#define N 16

int main() {
  int i;
  int ty[N];
  int tx[N];
  int tmp = 5;
  tx[i] = 9;
  for(i = 0; i < N/2; i++) {
    tx[i] = 1;
  }
  for(i = N/2; i < N; i++) {
    tx[i] = 2;
  }
  for(i = 0; i < N; i++) {
    ty[i] = 2*N + 1;
  }
  for(i = N/4; i < 3*N/4; i++) {
    ty[i] = (N + 5)*tx[i] + ty[i];
  }
  for (i = 8; i < 9; i++) {
    tmp = ty[i];
    ty[i] = tx[i];
    tx[i] = tmp;
  }
  for (i = 67; i < 90; i++) {
    tmp = ty[i];
    ty[i] = tx[i];
    tx[i] = i;
  }
  return 0;
}
