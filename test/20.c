#include <stdio.h>
#define M 16
#define N 8

int main() {
  int mat[M][N];
  int vec[N];
  int y[N];
  int i;
  int j;
  int plop = 13;
  int tmp;
  for (i = 0; i < M; i++) {
    tmp = y[i] * plop;
    for (j = 0; j < N; j++) {
      y[i] = ((2 * mat[i][j]) * vec[j]);
    }
    y[i] = y[i] + tmp;
  }

  for (i = 0; i < M; i++) {
    y[i] = y[i] * 15;
    for (j = 0; j < N; j++) {
      y[i] = ((2 * mat[i][j]) * vec[j]);
    }
  }
  return 0;
}
