#include <stdio.h>
#define M 16
#define N 8
#define FOO 38

int main() {
  int labonnemat[M][N];
  int lebonvec[N];
  int y[N];
  int i;
  int j;

  int a[M][M];
  int b[M][M];
  int c[M][M];
  int beta = 5;
  int alpha = 7;

  int k;
  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      c[i][j] *= beta + i;
      for (k = 0; k < M; k++) {
        c[i][j] += alpha * a[i][k] * b[k][j];
     }
    }
  }

  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      c[i][j] *= 40 * j;
      for (k = 0; k < M; k++) {
        c[i][j] += alpha * a[i][k] * b[k][j];
     }
    }
  }

  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      c[i][j] *=  k * 2 + 40;
      for (k = 0; k < M; k++) {
        c[i][j] += alpha * a[i][k] * b[k][j];
     }
    }
  }

  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      c[i][j] *= beta;
      for (k = 0; k < M; k++) {
        c[i][j] += alpha * a[i][k] * c[k][j];
     }
    }
  }

  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      c[i][j] *= beta;
      for (k = 0; k < M; k++) {
        c[i][j] += (alpha + j) * a[i][k] * b[k][j];
     }
    }
  }

  for (i = 0; i < M; i++) {
    for (j = 0; j < N; j++) {
      c[i][j] *= beta;
      for (k = 0; k < FOO; k++) {
        c[i][j] += (alpha) * a[i][k] * b[k][j];
     }
    }
  }
  return 0;
}
