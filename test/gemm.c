#include <stdio.h>
#define M 16
#define N 8

int main() {
  int labonnemat[M][N];
  int lebonvec[N];
  int y[N];
  int i;
  int j;

  int a[M][M];
  int b[M][M];
  int c[M][M];
  int beta = 5;
  int alpha = 7;

  for (i = 0; i < N; i++) {
    lebonvec[i] = i * 2;
    y[i] = i + 1;
  }
  for (i = 0; i < M; i++) {
    for (j = 0; j < N; j++) {
      labonnemat[i][j] = 420;
    }
  }
  int plop = 13;
  int tmp;
  for (i = 0; i < M; i++) {
    tmp = y[i] * plop;
    for (j = 0; j < N; j++) {
      y[i] = ((2 * labonnemat[i][j]) * lebonvec[j]);
    }
    y[i] = y[i] + tmp;
  }

  int k;
  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      c[i][j] *= beta;
      for (k = 0; k < M; k++) { c[i][j] += alpha * a[i][k] * b[k][j]; }
    }
  }
  return 0;
}
