---
title: "Projet BLASter"
subtitle: Compilation 2019-2020
date: "Team Bloster"
author:
  - LAJARGE Cyril
  - HENG Paul
  - OLIGER Timothée
  - OSSYWA Adrien
titlepage: true
logo: lobster.png
logo-width: 200
listings-no-page-break: true
toc-own-page: true
---

# Utilisation

## Make

Vous pouvez utiliser:
```sh
make && ./bin/blaster < test/13.c
```
et

```sh
make test
```
pour lancer l'ensemble des tests.

## Docker

Vous pouvez utiliser:
```sh
docker build -t blaster . && \
  docker run -i blaster < test/13.c
```

# Implémentation

Notre compilateur fonctionne de la manière suivante :

* Analyse lexicale (Lex).
* Analyse syntaxique (Yacc).
* Génération de l'AST (pendant l'analyse syntaxique).
* Génération de la table des symboles et analyse sémantique (à partir de l'AST).
* Traduction de l'AST en code (avec optimisations).

Les optimisations sont détectées lors de la traduction en code, afin de ne pas parcourir plusieurs fois l'AST.

## AST

L'arbre de syntaxe abstraite est implémenté à l'aide d'une ```union``` et d'une variable qui permet de connaître le type de l'arbre et donc les éléments de l'union qui sont définis.

```c
typedef struct ast {
  ast_type type;
  union {
    int number;
    double number_double;
    char* id;
    char* string;
    struct { // binary op == left binary_op_t right
      binary_op_t op_type;
      struct ast* left;
      struct ast* right;
    };
    struct { // unary op == ID unary_op_t
      unary_op_t unary_op_type;
      struct ast* identifier;
    };
    struct{ //== table id
      struct ast* tab_id;
      struct ast* tab_el;
    };
    ...
    struct {
      struct ast* block;
      struct ast* return_expr;
    };
  };
} ast;
```

Des fonctions sont définies afin de créer des arbres de chaque type. Elles sont utilisées dans le fichier Yacc afin d'étoffer l'AST complet du code au fur et à mesure de la lecture du fichier.

## TOS

La table des symboles est implémentée sous la forme d'une liste chainée.

```c
typedef struct s_entry{
  char* id;
  char* type;
  char* value;
  char* scope;
  struct s_entry* next;
} Symbol;

typedef struct{
  Symbol* head;
  int size;
} Tos;
```

Elle contient 5 champs:

- **id** qui contient l'id de la variable. Pour les nombres qui ne sont pas assignés lors de déclaration, un nom de variable de la forme **tx** (**x** étant un nombre) est généré.
- **type** qui contient le type de la variable.
- **value** qui contient la valeur de la variable le cas échéant.
- **scope** qui spécifie le scope de la variable.
- **next** qui est un pointeur vers la variable suivante.

# Fonctionnement

Par défaut, le programme écrit sur la sortie standard.
Avec l'option `-o <fichier>`, le programme généré est écrit dans un fichier, et le header `spec.h` est ajouté, si le code a été optimisé.
Les options `--tos`, `--ast` et `--version` écrivent sur la sortie standard.

_Exemple :_

```c
make
./bin/blaster -o out.c test/1.c
```

# Capacités du compilateur

Les types `int` et `double` sont supportés ainsi que les tableaux multi-dimensionnels de ces types.
Les déclarations en chaîne ne sont **pas** supportées (ex: `int a,b,c;`).

Les opérateurs possibles sont **+**, **-**, **\***, **/**, **++** et **--**.
Les assignations possibles sont **+=**, **-=**, **\*=** et **/=**.

Les structures de contrôle supportées sont:

- les conditionnels `if` avec ou sans `else`.
- les boucles `while`.
- les boucles `for` de la forme suivante:
  - la première partie correspond à l’initialisation d’un itérateur.
  - la seconde à la condition d’arrêt de la boucle.
  - la troisième à la mise àjour de l’itérateur.
- les appels de fonctions y compris récursivement.
- la fonction `printf` lorsque ```stdio.h``` est inclus.
- les directives pré-processeur ```#incude``` et ```#define```.

Une analyse sémantique simple est également effectuée:

- la redéfinition de variables.
- l'utilisation de variables non-déclarées.
- l'utilisation de fonctions inexistantes.

# Fonctions implémentées

Notre fichier de spécification est le suivant

```c
#ifndef SPEC_H
#define SPEC_H


/* Level 0 */
void int_mulacc(int *acc, int b, int c);
void double_mulacc(double *acc, double b, double c);

/* Level 1 - Vec */
void int_vec_set(int lo, int hi, int* v, int val);
void int_vec_swap(int lo, int hi, int* v1, int* v2);
void int_axpy(int lo, int hi, int a, int* x, int* y);

/* Level 2 - VecMat */

void int_gemv(int lo, int nl, int nc,
  int alpha, int **mat, int *x, int beta, int *y);

/* Level 3 - MatMat */

void int_gemm(int m, int n, int k, int alpha, int** a,
                  int**b, float beta, int** c);

#endif
```

Nous avons donc une fonction par niveau, ainsi que les opérations d'affectation
et de swap sur les vecteurs.

Nous avons préféré ne faire que peu de fonctions mais de les faire bien,
en faisant bien attention à la détection de dépendances des opérations par
rapport aux itérateurs par exemple.

En effet, nous avons considéré les faux positifs comme bien plus handicapant que les faux négatifs étant donné qu'ils rendent le code
non fonctionnel.

Le typage est supporté de manière simple, c'est-à-dire que si l'élément principal (vecteur ou matrice modifiée) est un `double`,
la fonction pour les `double` est appelée.

# Détails particuliers

## gemv

Deux « patterns » sont supportés.

```c
for (i = 0; i < M; i++) {
  tmp = y[i] * 15;
  for (j = 0; j < N; j++) {
    y[i] = ((2 * mat[i][j]) * vec[j]);
  }
  y[i] = y[i] + tmp;
}

for (i = 0; i < M; i++) {
  y[i] = y[i] * 15;
  for (j = 0; j < N; j++) {
    y[i] = ((2 * mat[i][j]) * vec[j]);
  }
}
```

De même, en cas d'utilisation d'une variable temporaire, un élément de tableau
est également accepté s'il est « statique » (ex: `tab[1]` mais pas `tab[i]`);

## gemm

Le pattern suivant est reconnu:
```c
int i, j, k;
  for (i = 0; i < M; i++) {
    for (j = 0; j < M; j++) {
      c[i][j] *= beta;
      for (k = 0; k < M; k++) { c[i][j] += alpha * a[i][k] * b[k][j]; }
    }
  }
  return 0;
  ```

Cependant l'implémentation n'est pas flexible.
