FROM alpine AS build-env

RUN apk add gcc make libc-dev bison flex

COPY . /tmp/compilation

WORKDIR /tmp/compilation

ENV BUILD_ENV DOCKER

RUN make build


FROM scratch

WORKDIR /

COPY --from=build-env /tmp/compilation/bin/blaster .

CMD ["/blaster"]
