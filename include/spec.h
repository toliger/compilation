#ifndef SPEC_H
#define SPEC_H


/* Level 0 */
void int_mulacc(int *acc, int b, int c);
void double_mulacc(double *acc, double b, double c);

/* Level 1 - Vec */
void int_vec_set(int lo, int hi, int* v, int val);
void int_vec_swap(int lo, int hi, int* v1, int* v2);
void int_axpy(int lo, int hi, int a, int* x, int* y);

/* Level 2 - VecMat */

void int_gemv(int lo, int nl, int nc,
  int alpha, int **mat, int *x, int beta, int *y);

/* Level 3 - MatMat */

void int_gemm(int m, int n, int k, int alpha, int** a,
                  int**b, float beta, int** c);

#endif
