#ifndef OPTI_H
#define OPTI_H
#include "ast.h"

typedef enum blas_fct {
  BLAS_NOOPTI,
  BLAS_MULACC,
  BLAS_AXPY, BLAS_VECSET, BLAS_VECSWAP,
  BLAS_GEMV,
  BLAS_GEMM
} blas_fct;

struct valset {
  ast *left;
  ast *right;
};

struct mat2D_idx {
  struct ast *li; // line index
  struct ast *ni; // col index
};

struct opti_args {
  blas_fct type;
  struct ast *lo;
  struct ast *hi;
  union {
    struct {
      struct ast *vecset_v;
      struct ast *vecset_val;
    };
    struct {
      struct ast *vecswap_v1;
      struct ast *vecswap_v2;
    };
    struct {
      struct ast *axpy_a;
      struct ast *axpy_x;
      struct ast *axpy_y;
    };
    struct {
      struct ast *hi2;
      struct ast *gemv_a;
      struct ast *gemv_b;
      struct ast *gemv_m;
      struct ast *gemv_x;
      struct ast *gemv_y;
    };
    struct {
      struct ast *gemm_m;
      struct ast *gemm_n;
      struct ast *gemm_k;
      struct ast *gemm_alpha;
      struct ast *gemm_a;
      struct ast *gemm_b;
      struct ast *gemm_beta;
      struct ast *gemm_c;
    };
  };
};

// utils

int checkVec(ast *start);

/**
 * @brief checks if the sub-ast is a matrix 2D
 *
 * @param start
 * @return struct mat2D_idx
 */
struct mat2D_idx isMat(ast *start);

/**
 * @brief checks if the sub-ast is a vector
 *
 * @param start
 * @return int
 */
int isVec(ast *start);

/**
 * @brief checks if 2 ID are equivalent
 *
 * @param a
 * @param b
 * @return int
 */
int idcmp(ast *a, ast *b);

/**
 * @brief checks if the sub-ast sets a value.
 * Accepts both decl and affect, and tabs if allow_tab set to 1
 *
 * @param start
 * @param allow_tab
 * @return struct valset pointers to left and right members
 */
struct valset checkValSet(ast *start, int allow_tab);

int checkDependancy(ast *iter, ast* start);

/**
 * @brief checks if 2 vectors are equivalent
 *
 * @param a
 * @param b
 * @return int
 */
int veccmp(ast *a, ast*b);

ast *getTabID(ast *);

int checkUnary(ast *start, unary_op_t op);
int checkBinary(ast *start, binary_op_t op);

int checkMulAcc(ast *);
void printMulAcc(ast *, int);

struct opti_args checkLevel1(ast *);
int printOpti(struct opti_args);

struct opti_args checkLevel2(ast *);

struct opti_args checkLevel3(ast *);


#endif
