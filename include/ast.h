#ifndef AST_H
#define AST_H

#define BUFFER_SIZE 64
#define CODE_SIZE 1024

typedef enum out_type_e {
  OUT_CODE,
  OUT_AST,
  OUT_NOSEMI,
  OUT_NOCR
} out_t;

typedef enum ast_type {
  AST_ID, AST_INT, AST_DOUBLE, AST_STRING,
  AST_FOR, AST_IF, AST_WHILE,
  AST_BINARY,
  AST_DECL,
  AST_AFFECT,
  AST_INSTR,
  AST_UNARY,
  AST_TABLE,
  AST_TABLE_AFFECT,
  AST_TABLE_DECL,
  AST_INCLUDE,
  AST_DEFINE,
  AST_GINSTR,
  AST_FCT,
  AST_FCT_ARGS,
  AST_FCT_CALL,
  AST_FCT_CALL_ARGS,
  AST_BLOCK
  } ast_type;

typedef enum var_type_e {
  INT, DOUBLE
} var_type_t;


typedef enum binary_op_e {
  OP_ADD,
  OP_MINUS,
  OP_DIV,
  OP_MUL,
  OP_LT,
  OP_GT,
  OP_LTE,
  OP_GTE,
  OP_EQ,
  OP_NEQ,
} binary_op_t;

typedef enum unary_op_e {
  OP_INCR,
  OP_DECR,
} unary_op_t;

typedef struct ast {
  ast_type type;
  union {
    int number;
    double number_double;
    char* id;
    char* string;
    struct { // binary op == left binary_op_t right
      binary_op_t op_type;
      struct ast* left;
      struct ast* right;
    };
    struct { // unary op == ID unary_op_t
      unary_op_t unary_op_type;
      struct ast* identifier;
    };
    struct{ //== table id
      struct ast* tab_id;
      struct ast* tab_el;
    };
    struct { // == type table = ast
      var_type_t tab_type;
      struct ast* tab_decl_id;
      struct ast* tab_decl_right;
    };
    struct{ // == table = ast
      struct ast* tab_affect_id;
      char* tab_affect_op;
      struct ast* tab_affect_right;
    };
    struct { // == for (init; cond_for; step) body_for
      struct ast *init;
      struct ast *cond_for;
      struct ast *step;
      struct ast *body_for;
    };
    struct {  // == while(condition) body while
      struct ast* cond_while;
      struct ast* body_while;
    };
    struct { // == if cond_if body_if else else_body_if
      struct ast *cond_if;
      struct ast *body_if;
      struct ast *else_body_if;
    };
    struct { // affect
      struct ast *affect_id;
      char* affect_op;
      struct ast *affect_right;
    };
    struct { // == type id = ast
      var_type_t var_type;
      struct ast *decl_id;
      struct ast *decl_right;
    };
    struct { // == instrs
      struct ast *instr;
      struct ast *instrs;
    };
    struct { // == instr preproc DEFINE
      char* define_id;
      int define_value;
    };
    struct { // == instr preproc INCLUDE
      char includeType;
      char* hfile;
    };
    struct { // == ginstrs
      struct ast *g_instr;
      struct ast *g_instrs;
    };
    struct {
      char* fct_type;
      char* fct_name;
      struct ast *args;
      struct ast *bloc;
    };
    struct{
      struct ast* args_left;
      struct ast* args_right;
    };
    struct {
      struct ast* fct_id;
      struct ast* fct_args;
    };
    struct{
      struct ast* call_args_left;
      struct ast* call_args_right;
    };
    struct {
      struct ast* block;
      struct ast* return_expr;
    };
  };
} ast;

ast* ast_new_operation(binary_op_t, ast*, ast*);
ast* ast_new_operation_unary(unary_op_t, ast*);
ast* ast_new_integer(int);
ast* ast_new_double(double);
ast* ast_new_string(char*);
ast* ast_new_id(char*);
ast* ast_new_table_id(ast*, ast*);
ast* ast_new_for(ast*, ast*, ast*, ast*);
ast* ast_new_while(ast*, ast*);
ast* ast_new_if(ast*, ast*, ast*);
ast* ast_new_table_decl(var_type_t, ast*, ast*);
ast* ast_new_table_affect(ast*, char*, ast*);
ast* ast_new_decl(var_type_t, ast*, ast*);
ast* ast_new_affect(ast*, char*, ast *);
ast* ast_new_instrs(ast*, ast *);
ast* ast_new_define(char*, int);
ast* ast_new_include(char,char*);
ast* ast_new_ginstrs(ast*, ast*);
ast* ast_new_fct(char*, char*, ast*, ast*);
ast* ast_new_fct_args(ast*, ast*);
ast* ast_new_fct_call(ast*, ast*);
ast* ast_new_fct_call_args(ast*, ast*);
ast* ast_new_block(ast*, ast*);

var_type_t str2type(char *);
char* vt2str(var_type_t type);
char* generateVariable();
void ast_print(ast*, int, out_t);
void ast_free(ast*);

#endif
