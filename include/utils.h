#ifndef UTILS_H
#define UTILS_H
#include <stdio.h>
#include <stdarg.h>

extern FILE* outstream;
extern char optimized;

void print_out(const char *fmt, ...);

#endif
