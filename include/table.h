#ifndef TABLE_H
#define TABLE_H
#include "ast.h"

#define STRING_SIZE 30

typedef struct s_entry{
  char* id;
  char* type;
  char* value;
  char* scope;
  //.. add more table rows here
  struct s_entry* next;
} Symbol;

typedef struct{
  Symbol* head;
  int size;
} Tos;

Tos* tos_create();
Symbol* symbol_allocate();
Symbol* symbol_new_temp();
Tos* tos_new_int(Tos* t, int value, char* type, char* scope);
Tos* tos_new_double(Tos* t, double value, char* type, char* scope);
Tos* tos_new_var(Tos* t, char* type, char* id, char* value, char* scope);
Tos* tos_add_symbol(Tos* t, Symbol* s);
Symbol* tos_lookup(Tos* t, char* identifier);
void ast_to_tos(ast* ast, Tos** t, char* scope, char* type, int decl);
void tos_print(Tos* t);
void symbol_free(Symbol* s);
void tos_free(Tos* t);

Tos* table;

#endif
