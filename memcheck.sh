#!/bin/bash
for i in $(ls test)
do
  valgrind --leak-check=full --error-exitcode=240 ./bin/blaster test/$i > /dev/null 2>&1;
  echo "$? $i";
done
