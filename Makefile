# General
CC			= gcc
LD			= gcc
RM			= rm -rf
RMDIR		= rmdir
INSTALL		= install

TARGET		= blaster
SRCDIR		= src
OBJDIR		= obj
BINDIR		= bin
HDIR      = include
LYNAME		= ensemble
ARTDIR		= artefacts

CFLAGS		:= -Iinclude -Iartefacts -Wall -Wextra -Werror
LDFLAGS 	:=

ifeq ($(BUILD_ENV), DEV)
	CFLAGS += -g
else
	CFLAGS += -O3
endif
ifeq ($(BUILD_ENV), DOCKER)
	CFLAGS += -static
endif


SRC			:= $(wildcard $(SRCDIR)/*.c)
HEADERS  := $(wildcard $(HDIR)/*.h)
OBJ			:= $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(SRC:.c=.o))
DEP			:= $(OBJ:.o=.d)
BIN			:= $(BINDIR)/$(TARGET)
OYACC 	:= $(OBJDIR)/$(LYNAME).tab.o
OLEX 	:= $(OBJDIR)/$(LYNAME).yy.o
-include $(DEP)

# Build Rules
.PHONY: clean
.DEFAULT_GOAL := build

.PHONY: build
build: setup $(BIN)

setup: dir

dir:
	@mkdir -p $(OBJDIR)
	@mkdir -p $(BINDIR)
	@mkdir -p $(ARTDIR)

foo:
	echo $(OYACC)

$(BIN): $(OYACC) $(OLEX) $(OBJ)
	$(LD) $(CFLAGS) $(LDFLAGS) $^ -o $@

$(OYACC): $(ARTDIR)/$(LYNAME).tab.c
	$(CC) -Iartefacts -Iinclude -c -o $@ $<

$(OBJDIR)/%.yy.o: $(ARTDIR)/%.yy.c
	$(CC) -Iartefacts -Iinclude -c -o $@ $<

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) -c -o $@ $<

$(ARTDIR)/$(LYNAME).tab.c: yacc

$(ARTDIR)/$(LYNAME).yy.c: lex

yacc:
	yacc -d -o $(ARTDIR)/$(LYNAME).tab.c $(SRCDIR)/yacc/$(LYNAME).y

lex:
	flex -o $(ARTDIR)/$(LYNAME).yy.c $(SRCDIR)/lex/$(LYNAME).l

clean:
	$(RM) $(OBJDIR) $(BINDIR) $(ARTDIR)

.PHONY: clean_all
clean_all: clean
	$(RM) html latex rapport/rapport.pdf

.PHONY: format
format:
	clang-format -i $(SRC) $(HEADERS)

.PHONY: run
run:
	./$(BIN)

.PHONY: test
test: setup
	find $(BINDIR) -name $(TARGET) 2> /dev/null;
	for f in `ls test`; do ./bin/blaster <  ./test/$$f > /dev/null 2> /dev/null\
		&& echo "test $$f OK" \
		|| echo "test $$f failed"; done
	for f in `ls wrongtest`; do ./bin/blaster <  ./wrongtest/$$f > /dev/null 2> /dev/null\
		&& echo "test $$f failed" \
		|| echo "test $$f OK"; done

.PHONY: doc
doc:
	doxygen

.PHONY: rapport
rapport:
	pandoc rapport/rapport.md -o rapport/rapport.pdf --pdf-engine=xelatex
